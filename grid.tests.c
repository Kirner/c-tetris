#include <stdint.h>
#include <stdbool.h>

#include "unity.h"

#include "grid.h"

void test_Drawing_a_shape_on_a_grid_sets_appropriate_cells_in_the_grid(void)
{
  const uint32_t shapeWidth = 3, shapeHeight = 2;
  const uint32_t gridWidth = 4, gridHeight = 4;
  const Grid_CellT shape[] = {
    Grid_CellT_On,  Grid_CellT_On, Grid_CellT_On,
    Grid_CellT_Off, Grid_CellT_On, Grid_CellT_Off
  };

  Grid_Init(gridWidth, gridHeight);
  int32_t result = Grid_Draw(0, 0, shapeWidth, shapeHeight, shape);

  const Grid_CellT const *gridPtr = Grid_GetGridPtr();

  TEST_ASSERT_TRUE(result >= 0);

  for (size_t row = 0; row < gridHeight; row++)
  for (size_t col = 0; col < gridWidth; col++) {
    if (row <= shapeWidth && col <= shapeHeight) {
      TEST_ASSERT_EQUAL(shape[row * shapeWidth+ col], gridPtr[row * shapeWidth+ col]);
    } else {
      TEST_ASSERT_EQUAL(Grid_CellT_Off, gridPtr[row * shapeWidth+ col]);
    }
  }
}
