.POSIX:

include config.mk

SourceFiles := tetris.c grid.c
ObjectFiles := ${SourceFiles:.c=.o}

UnitySourceFiles := Unity/src/*.c

TestsSourceFiles := grid.tests.c 

.PHONY: options clean

options:
	@echo Compiler      = ${Compiler}
	@echo CompilerFlags = ${CompilerFlags}
	@echo LinkerFlags   = ${LinkerFlags}

.c.o:
	${Compiler} ${CompilerFlags} -c -o $@ $<

${ObjectFiles}: ${SourceFiles}

tetris: ${ObjectFiles}
	${Compiler} -o $@ ${ObjectFiles} ${LinkerFlags}

tetris.tests:
	ruby Unity/auto/generate_test_runner.rb ${TestsSourceFiles} testRunner.c 
	gcc ${TestCompilerFlags} ${TestsSourceFiles} ${SourceFiles} ${UnitySourceFiles} testRunner.c \
	   -o bin/tetris.tests

run-tests: tetris.tests
	./bin/tetris.tests

clean:
	rm -f *.o tetris tetris.tests
