#ifndef GRID_H
#define GRID_H

#include <stdint.h>

typedef enum{
  Grid_CellT_On,
  Grid_CellT_Off
} Grid_CellT;

void    Grid_Init(int, int);
int32_t Grid_Draw(uint32_t x, uint32_t y, uint32_t width, uint32_t height, const Grid_CellT *data);
const Grid_CellT const *Grid_GetGridPtr();

#endif
