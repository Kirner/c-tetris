#include "grid.h"

void
Grid_Init(int width, int height) {
	(void) width;
	(void) height;
}

int32_t
Grid_Draw(uint32_t x, uint32_t y, uint32_t width, uint32_t height, const Grid_CellT *data)
{ 
	(void) x;
	(void) y;
	(void) width;
	(void) height;
	(void) data;
	return -1;
}

const Grid_CellT const *
Grid_GetGridPtr()
{ 
	Grid_CellT *_ = Grid_CellT_On;
	(void) _;
	return _;
}
