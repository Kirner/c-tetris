Version           = 0.0

Compiler          = /usr/bin/cc

Includes          = -I /usr/include
Libraries         = -L /usr/lib

TestIncludes      = -I Unity/src

CompilerFlags     = ${Includes} -Wall -Wextra -O2
LinkerFlags       = ${Libraries}

TestCompilerFlags = --std=c99 --pedantic -Wall -Wextra -O0 ${Includes} ${TestIncludes}
TestLinkerFlags   = ${LinkerFlags}
